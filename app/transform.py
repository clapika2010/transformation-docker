import argparse
import os
import subprocess
import sys

parser = argparse.ArgumentParser(
    description='evaluate tagging results using CoNLL criteria',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
arg = parser.add_argument
arg('-i', '--input', metavar='STR', default="0",
    help='Input format. 0 for LDAS, 1 for PIHM, 2 for Cycles (Not available)')

arg('-o', '--output', metavar='STR', default="0",
    help='Output format. 0 for LDAS (Not available), 1 for PIHM, 2 for Cycles')

arg('-i', '--input', metavar='STR', default="0", help='Input path')

arg('-o', '--output', metavar='STR', default="0", help='Output path')

args = parser.parse_args(sys.argv[1:])
args.input = int(args.input)
args.output = int(args.output)

if args.input == 2:
    print("Input format cannot be Cycles")
    exit(1)
if args.output == 0:
    print("Output format cannot be LDAS")
    exit(1)
if args.input == args.output:
    print("Output and input formats should be different")
    exit(1)

if args.input == 0 and args.output == 1:
    os.chdir("/app")
    subprocess.call("bash LDAS-PIHM-transformation.sh", shell=True)

elif args.input == 0 and args.output == 2:
    os.chdir("/app")
    subprocess.call("python FLDAS-Cycles-transformation.py test transform", shell=True)

elif args.input == 1 and args.output == 2:
    subprocess.call("python PIHM-Cycles-transformation.py test transform", shell=True)
