#!/bin/bash

set -e

# we need FLDAS_NOAH01_A_EA_D.001/

# as well as PIHM-base
python PIHM-data-find.py
tar xzf data/PIHM_base.tar.gz

# figure out the start/end years
START_YEAR=`grep ^start_year data/mint_run_config | sed 's/.*= *//'`
END_YEAR=`grep ^end_year data/mint_run_config | sed 's/.*= *//'`

# now do the transform - output is pihm.forc
Rscript FLDAS-to-PIHM.R ${START_YEAR} ${END_YEAR}

