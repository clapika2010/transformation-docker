FROM mintproject/base-ubuntu18

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y build-essential && \
    apt-get install -y software-properties-common && \
    apt-get install -y apt-transport-https

#RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
#RUN add-apt-repository 'deb [arch=amd64,i386] https://cran.rstudio.com/bin/linux/ubuntu xenial/'
#RUN add-apt-repository -y ppa:ubuntugis/ppa
#RUN apt-get update
#RUN apt-get install -y r-base libgdal1-dev libproj-dev gdal-bin proj-bin


RUN apt-get install -y python3-pip python3-dev && \
    ln -s /usr/bin/python3 /usr/local/bin/python

RUN Rscript -e "install.packages('raster')"
RUN Rscript -e "install.packages('rgdal')"
RUN Rscript -e "install.packages('rgeos')"
RUN Rscript -e "install.packages('xts')"
RUN Rscript -e "install.packages('ncdf4')"
RUN python -m pip install -U pip
RUN python -m pip install netCDF4==1.4.1 python-dateutil==2.7.1 requests

RUN wget -nv https://computation.llnl.gov/projects/sundials/download/sundials-2.2.0.tar.gz && \
    tar xvf sundials-2.2.0.tar.gz && \
    cd sundials && \
    ./configure --prefix=/opt/sundials && \
    make && \
    make install && \
    cd .. && \
    rm -rf sundials*

RUN wget -nv http://www.pihm.psu.edu/PIHM_v2.2.tar && \
    tar xvf PIHM_v2.2.tar && \
    cd PIHM_v2.2 && \
    (make clean || true) && \
    (grep -rl Makefile . | xargs sed -i 's;/Users/xxy113/software;/opt;g') && \
    make pihm && \
    mv pihm /usr/bin && \
    cd .. && \
    rm -rf PIHM*

COPY ./app /app
WORKDIR /app


ENTRYPOINT ["python", "transform.py"]
