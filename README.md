# Transformation Docker

## How to build docker image:

```
    docker build . -t docker_name
```



## How to run docker with transformation

To run docker with transformation, necessary data should be stored in /app/data folder. Parameterizing this path will be done soon. Sample data can be downloaded from https://scitech.isi.edu/mint/

```
    docker run -v local_host_data_folder:/app/data docker_name [-i input_format] [-o output_format]
```

Parameters:
* `[-i input_format]:  0 for LDAS, 1 for PIHM, 2 for Cycles (Not available) (default: 0)`
* `[-o output_format]:  0 for LDAS (Not available), 1 for PIHM, 2 for Cycles (default: 0)`